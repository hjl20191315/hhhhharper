# 分组密码及其工作模式
### 一、概述

对称密码：加密密钥和解密密钥两者可以互推出来。大多数情况下，加密和解密的密钥是相同的。

对称密码的分类

1.分组密码：对明文进行分块，再对每一块进行加密；利用分组密码，相同的明文用相同的密钥加密永远得到相同的密文。

2.序列密码：也称流密码，加密时每次加密一位或一个字节明文；利用序列密码，每次对相同的明文位或字节加密都会得到不同的密文位或字节。



### 二、分组密码

分组密码的常见算法：AES、DES、SM4...



#### 填充（Padding）方式

假设块长度为8字节，要加密的明文数据长度为9字节。那么消息被切成两个块，第二块只有1字节，需要填充7个字节。假定9字节的明文数据为：F1 F2 F3 F4 F5 F6 F7 F8 F9。

1.Zero填充：全部填充为0的字节，结果如下：

                    F1 F2 F3 F4 F5 F6 F7 F8     //第一块
                    F9 00 00 00 00 00 00 00     //第二块

2.X923填充：填充为0的字节序列，最后一个字节记录填充的总字节数，结果如下：

                    F1 F2 F3 F4 F5 F6 F7 F8     //第一块
                    F9 00 00 00 00 00 00 07     //第二块

3.PKCS7填充：每个填充的字节都记录了填充的总字节数，结果如下：

                    F1 F2 F3 F4 F5 F6 F7 F8     //第一块

                    F9 07 07 07 07 07 07 07     //第二块

4.ISO10126填充：填充随机字节序列，最后一个字节记录填充的总字节数，结果如下：

                    F1 F2 F3 F4 F5 F6 F7 F8     //第一块

                    F9 EF F7 3A 7D 75 F6 07     //第二块



#### 分组密码的工作模式

+ ECB：Electronic Code Book，电子密码本模式
+ CBC：Cipher Block Chaining，密码分组链接模式
+ CFB：Cipher FeedBack，密码反馈模式
+ OFB：Output FeedBack，输出反馈模式
+ CTR：CounTeR，计数器模式

1.ECB

加密过程

![](https://img2022.cnblogs.com/blog/1813289/202203/1813289-20220312224103542-374750060.png)

解密过程

![](https://img-blog.csdn.net/20180327152941121?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

加密方式：一个明文分组加密成一个密文分组，每个明文分组可被独立地不按次序地进行加密。

优点：简单，有利于并行计算，误差不会被传送。
如果密文中数据位出错，解密时，就会使得相对应的整个明文分组解密错误，但不会影响其它明文。然而，如果密文中偶尔丢失或添加一些数据位，那么整个密文序列将不能正确解密，除非有某种帧结构能够重新排列分组的边界。

缺点：不隐藏明文的模式；可能对明文进行主动攻击。如果密码分析者具有很多消息的明文和密文，那他就可以在不知道密钥的情况下编辑密码本。

在许多实际情况中，消息格式趋于重复，不同的消息可能会有一些位序列是相同的，比如电子邮件就可能有固定的结构，而这些消息在很大程度上是冗余的或者有一个很长的0和空格组成的字符串。如果加密的消息具有一些冗余消息，那么这些信息趋向于在不同消息的同一位置出现，密码分析者就可以获得很多信息，可推断出使用了ECB加密，从而对明文发动攻击。加密消息块相互独立称为被攻击的弱点。

2.CBC

![](https://img-blog.csdn.net/20180327150024893?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

加密过程

![](https://img-blog.csdn.net/20180327152906330?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

解密过程

加密方式：使用了初始化向量（IV）和第一组明文进行异或操作，然后加密，加密后的密文与下一组明文进行异或操作，以此类推。这种方式保证了每组密文依赖于它前面所有的明文。同时，为了保证每条消息的唯一性，在第一块中需要使用初始化向量。

缺点：加密过程是串行的，无法被并行化，而且消息必须被填充到块大小的整数倍。解决后一个问题的一种方法是利用密文窃取。

注意在加密时，明文中的微小改变会导致其后的全部密文块发生改变，而在解密时，从两个邻近的密文块中即可得到一个明文块。因此，解密过程可以被并行化，而解密时，密文中一位的改变只会导致其对应的明文块和下一个明文块中对应位发生改变，不会影响到其它明文的内容。

3.CFB

![](https://img-blog.csdn.net/20180327151158799?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

加密过程

![](https://img-blog.csdn.net/20180327153040166?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

解密过程

分组密码算法也可以用于自同步序列密码，就是所谓的密码反馈模式。

加密方式：与CBC相比，初始化向量（IV）和明文颠倒了一下位置。因此与CBC不同，CFB不会先进行初始化向量和第一组明文的异或操作后进行加密，然后密文与下一组明文再进行异或后加密操作，而是它会首先对初始化向量进行加密，然后由加密后的初始化向量与明文进行异或操作生成密文，接着再对这个密文进行加密，然后与下一组明文进行异或操作。

特点：其自同步特性仅仅与CBC相同，即若密文的一整块发生错误，CBC和CFB都能解密大部分数据，而仅有一位数据错误。若需要在仅有了一位或一字节错误的情况下也让模式具有自同步性，必须每次只加密一位或一字节。可以将移位寄存器作为块密码的输入，以利用CFB的自同步性。

4.OFB

![](https://img-blog.csdn.net/20180327151436804?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

加密过程

![](https://img-blog.csdn.net/2018032715361152?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

解密过程

输出反馈（OFB）模式是将分组密码作为同步序列密码运行的一种方法。

加密方式： 与CFB非常相似。它们的不同在于加密后的初始化向量没有与明文进行异或操作。事实上对于第一组明文，初始化向量加密以后作为第二组明文的输入并且再与第一组明文进行异或操作。后续的加密操作都发生在异或之前。

特点：输出反馈模式可以将块密码变成同步的流密码。它产生密钥流的块，然后将其与明文块进行异或，得到密文。与其它流密码一样，密文中一个位的翻转会使明文中同样位置的位也产生翻转。这种特性使得许多错误校正码（比如奇偶校正位），即使在加密前计算而在加密后进行校验也可以得出正确结果。

5.CTR

![](https://img-blog.csdn.net/20180327155717570?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

加密过程

![](https://img-blog.csdn.net/20180327155738608?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

解密过程

特点：与OFB相似，CTR将块密码变为流密码。它通过递增一个加密计数器以产生连续的密钥流，其中，计数器可以是任意保证不产生长时间重复输出的函数，但使用一个普通的计数器是最简单和最常见的做法。使用简单的、定义好的输入函数是有争议的：批评者认为它“有意的将密码系统暴露在已知的、系统的输入会造成不必要的风险”。目前，CTR已经被广泛的使用了，由输入函数造成的问题被认为是使用的块密码的缺陷，而非CTR模式本身的弱点。

CTR模式的特征类似于OFB，但它允许在解密时进行随机存取。由于加密和解密过程均可以进行并行处理，CTR适合运用于多处理器的硬件上。

CTR模式是一种通过将逐次累加的计数器进行加密来生成密钥流的流密码，在CTR模式中，每个分组对应一个逐次累加的计数器，并通过对计数器进行加密来生成密钥流。最终的密文分组是通过将计数器加密得到的比特序列与明文分组进行XOR而得到的，如下图所示：

![](https://img-blog.csdn.net/20180403154116611?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

![](https://img-blog.csdn.net/20180403154133475?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMxODI1NTY5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
