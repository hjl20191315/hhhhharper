#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/err.h>
#include <openssl/objects.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/pkcs12.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>

int main(int argc, char *argv[])
{
    BIO *bio;
	int rv;
	PKCS12 *p12 = NULL;					//保存发送者私钥的PKCS12结构体指针
	X509 *RecvCert = NULL;				//保存接收者证书的X509结构体指针
	EVP_PKEY *pkey = NULL;				//保存发送者私钥的EVP_PKEY结构体指针
	X509 *SignCert = NULL;				//保存发送者证书的X509结构体指针
	unsigned char *tmp;
	unsigned char recvCertBuf[4096];	//保存接收者证书的数组
	unsigned int recvCertBufLen;		//接收者证书长度
	unsigned char PfxBuf[4096];			//保存发送者P12文件的数组
	unsigned int PfxBufLen;				//p12文件长度
	unsigned char Buffer[4096];			//保存待处理的文件数据的数组
	unsigned int BufferLen;				//数据长度
	unsigned char SessionKey[128];		//加密发送文件的会话密钥
	unsigned char CipherSessionKey[256];//会话密钥的密文
	unsigned int CipherSessionKeyLen;	//会话密钥的密文长度
	unsigned char Digest[512];			//待发送文件的摘要值
	unsigned int DigestLen;				//摘要长度
	long unsigned int siglen;               //签名长度
	EVP_MD_CTX *mdctx;					//计算摘要的上下文
	FILE *fp;							//文件句柄
	EVP_PKEY_CTX *Signctx;              //rsa签名上下文
	unsigned char *sig;       //签名值
	char err[1024];


	OpenSSL_add_all_algorithms();
	mdctx = EVP_MD_CTX_new();
	//打开接收者证书
	fp = fopen(argv[1],"rb");
	if(fp==NULL)
    {
        fprintf(stderr,"File to open %s\n", argv[1]);
        return 0;
    }
	recvCertBufLen = fread(recvCertBuf,1,4096,fp);
	fclose(fp);
	//打开发送者PFX（p12）文件
	fp = fopen(argv[2],"rb");
	if(fp==NULL)
    {
        fprintf(stderr,"File to open %s\n", argv[2]);
        return 0;
    }
	PfxBufLen = fread(PfxBuf,1,4096,fp);
	fclose(fp);

	//把接收者证书转化为X509结构体
	tmp = recvCertBuf;
	RecvCert = d2i_X509(NULL,(const unsigned char **)&tmp,recvCertBufLen);
	if(RecvCert == NULL)
	{
	    fprintf(stderr,"File to transform %s\n", argv[1]);
		return 0;
	}

	//把P12文件转化为PKCS12结构体
	bio = BIO_new(BIO_s_mem());
	rv = BIO_write(bio,PfxBuf,PfxBufLen);
	p12 = d2i_PKCS12_bio(bio, NULL);
	if(p12 ==NULL)
	{
		X509_free(RecvCert);
		BIO_free_all(bio);
		fprintf(stderr,"File to transform %s\n", argv[2]);
		return 0;
	}
	BIO_free_all(bio);

	char Passwd[1024];
	printf("请输入pkcs12证书密码\n");
	scanf("%s",Passwd);
	//从PKCS12结构体中解析获得私钥和证书
	rv = PKCS12_parse(p12, Passwd,&pkey,&SignCert,NULL);
	if(rv != 1)
	{
		X509_free(RecvCert);
		PKCS12_free(p12);
		printf("密码错误!\n");
		return 0;
	}

	//对待发送文件签名
	mdctx = EVP_MD_CTX_new();
	EVP_MD_CTX_init(mdctx);	//初始化摘要上下文
	if(!EVP_SignInit_ex(mdctx, EVP_sha256(), NULL))	//设置摘要算法，这里选择SM3
	{
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		printf("初始化摘要失败！\n");
		return 0;
	}
	//打开待发送的文件
	fp = fopen(argv[3],"rb");
	if(fp == NULL)
	{
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		return 0;
	}
	//不断循环，以4096字节为单位读取文件，并摘要
	for(;;)
	{
		BufferLen=fread(Buffer,1,4096,fp);//每次读取4096个字节
		if(BufferLen <=0)
			break;
		if(!EVP_SignUpdate(mdctx, Buffer, BufferLen))//摘要
		{

			EVP_PKEY_free(pkey);
			X509_free(SignCert);
			X509_free(RecvCert);
			PKCS12_free(p12);
			fclose(fp);
			printf("签名摘要生成失败EVP_SignUpdate\n");
			return 0;
		}
	}
	fclose(fp);
	if(!EVP_DigestFinal(mdctx,Digest,&DigestLen))//完成摘要，获得摘要值
	{
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		ERR_error_string(ERR_get_error(),err);
		printf("签名摘要生成摘要失败EVP_SignFinal %s\n",err);
		return 0;
	}
	//签名开始
	Signctx = EVP_PKEY_CTX_new(pkey, NULL /* no engine */);
      if (!Signctx)
      {
          fprintf(stderr,"EVP_PKEY_CTX_new Fail!");
          EVP_PKEY_free(pkey);
          X509_free(SignCert);
		  X509_free(RecvCert);
		  PKCS12_free(p12);
      }
      if (EVP_PKEY_sign_init(Signctx) <= 0)
      {
          fprintf(stderr,"EVP_PKEY_sign_init Fail!");
          EVP_PKEY_free(pkey);
          X509_free(SignCert);
		  X509_free(RecvCert);
		  PKCS12_free(p12);
      }
      if (EVP_PKEY_CTX_set_rsa_padding(Signctx, RSA_PKCS1_PADDING) <= 0)
      {
          fprintf(stderr,"EVP_PKEY_CTX_set_rsa_padding Fail!");
          EVP_PKEY_free(pkey);
          X509_free(SignCert);
		  X509_free(RecvCert);
		  PKCS12_free(p12);
      }
       if (EVP_PKEY_CTX_set_signature_md(Signctx, EVP_sha256()) <= 0)
       {
           fprintf(stderr,"EVP_PKEY_CTX_set_signature_md Fail!");
           ERR_error_string(ERR_get_error(),err);
           printf("%s\n",err);
          EVP_PKEY_free(pkey);
          X509_free(SignCert);
		  X509_free(RecvCert);
		  PKCS12_free(p12);
       }
       if (EVP_PKEY_sign(Signctx, NULL, &siglen, Digest, DigestLen) <= 0)
       {
           fprintf(stderr,"EVP_PKEY_sign Fail!");
          EVP_PKEY_free(pkey);
          X509_free(SignCert);
		  X509_free(RecvCert);
		  PKCS12_free(p12);
       }

       sig = OPENSSL_malloc(siglen);
       if (!sig)
       {
           fprintf(stderr,"OPENSSL_malloc Fail!");
           EVP_PKEY_free(pkey);
          X509_free(SignCert);
		  X509_free(RecvCert);
		  PKCS12_free(p12);
       }
       if (EVP_PKEY_sign(Signctx, sig, &siglen, Digest, DigestLen) <= 0)
       {
          fprintf(stderr,"EVP_PKEY_sign Fail!");
          EVP_PKEY_free(pkey);
          X509_free(SignCert);
		  X509_free(RecvCert);
		  PKCS12_free(p12);
       }


	/*unsigned char real_Sign[512];
	unsigned int real_Sign_Len;
	real_Sign_Len = EVP_PKEY_encrypt_old(real_Sign,Sign,SignLen,pkey);
*/
/*
	if(!EVP_SignFinal(mdctx,Sign,&SignLen,pkey))//完成签名，获得签名值
	{
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		ERR_error_string(ERR_get_error(),err);
		printf("签名失败EVP_SignFinal %s\n",err);
		return 0;
	}
*/

	//释放密钥和证书以及pkcs12结构体
	EVP_PKEY_free(pkey);
	X509_free(SignCert);
	PKCS12_free(p12);

	//产生随机数，作为会话密钥
	RAND_bytes(SessionKey,128);
	//使用接收者证书公钥加密会话密钥
	CipherSessionKeyLen = EVP_PKEY_encrypt_old(CipherSessionKey,SessionKey,128,X509_get_pubkey(RecvCert));
	if(CipherSessionKeyLen <= 0)
	{
		X509_free(RecvCert);
		return 0;
	}
	X509_free(RecvCert);
	//利用会话密钥加密原文，并输出到密文到文件
	FILE *fpIn;
	FILE *fpOut;
	fpIn = fopen(argv[3],"rb");
	if(fpIn == NULL)
	{
		return 0;
	}

	fpOut = fopen(argv[4],"wb");
	if(fpOut == NULL)
	{
		return 0;
	}

	//密文文件格式：
	// |------------------|--------|------------------------|------------|--------------
	// |签名信息长度4Bytes|签名信息|会话密钥的密文长度4Bytes|会话密钥密文|原文数据的密文
	fwrite(&siglen,1,sizeof(siglen),fpOut);//写签名长度到文件
	fwrite(sig,1,siglen,fpOut);			//写签名值到文件
	fwrite(&CipherSessionKeyLen,1,sizeof(CipherSessionKeyLen),fpOut);	//写密文的会话密钥的长度到文件
	fwrite(CipherSessionKey,1,CipherSessionKeyLen,fpOut);//写密文的会话密钥到文件

	EVP_CIPHER_CTX *ctx;
	ctx = EVP_CIPHER_CTX_new();
	unsigned char out[1024];
	int outl;
	unsigned char in[1024];
	int inl;
	EVP_CIPHER_CTX_init(ctx);//初始化密码算法上下文
	//设置密码算法和密钥，这里采用128位的sm4算法。
	rv = EVP_EncryptInit_ex(ctx,EVP_sm4_ecb(),NULL,SessionKey,NULL);
	if(rv != 1)
	{
		EVP_CIPHER_CTX_cleanup(ctx);
		return 0;
	}
	//以1024字节为单位，循环读取原文，并加密，然后输出到密文文件。
	for(;;)
	{
		inl = fread(in,1,1024,fpIn);//读取1024字节
		if(inl <= 0)
			break;
		rv = EVP_EncryptUpdate(ctx,out,&outl,in,inl);//加密
		if(rv != 1)
		{
			fclose(fpIn);
			fclose(fpOut);
			EVP_CIPHER_CTX_cleanup(ctx);
			return 0;
		}
		fwrite(out,1,outl,fpOut);//输出密文到文件
	}
	rv = EVP_EncryptFinal_ex(ctx,out,&outl);//完成加密，输出剩余的密文。
	if(rv != 1)
	{
		fclose(fpIn);
		fclose(fpOut);
		EVP_CIPHER_CTX_cleanup(ctx);
		return 0;
	}
	fwrite(out,1,outl,fpOut);//写文件
	fclose(fpIn);
	fclose(fpOut);
	EVP_CIPHER_CTX_cleanup(ctx);
	return 0;
}
