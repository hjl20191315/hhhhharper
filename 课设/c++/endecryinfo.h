#ifndef ENDECRYINFO_H
#define ENDECRYINFO_H

#include <iostream>
#include "openssl/err.h"
#include "openssl/objects.h"
#include "openssl/evp.h"
#include "openssl/x509.h"
#include "openssl/pem.h"
#include "openssl/err.h"
#include "openssl/pkcs12.h"
#include "openssl/rand.h"
#include "openssl/rsa.h"
#include <tuple>
using namespace std;

class EnDecryInfo
{
public:
    EnDecryInfo();
    //!生成密文文件
    tuple<bool, string> encryptionFile(const string& infilePath, const string& outfilePath);
    //!生成解密文件
    tuple<bool, string> decryptionFile(const string& infilePath, const string& outfilePath);
    //!验证签名
    string checkSign();
    //!加密端处理
    string checkPasswd_encry(const string& passwd, const string& sendFile, const string& receFile);
    //!解密端处理
    string checkPasswd_decry(const string& passwd, const string& sendFile, const string& receFile);

private:
    //!加载接收者证书
    bool loadReceLicense(const string& filePath);
    //!加载发送者PFX（p12）文件
    bool loadSendLincense(const string& filePath);
    //!加载待发送的文件
    string loadWaitSendFile(const string& filePath);
    //!从PKCS12结构体中解析获得私钥和证书
    //! @return "" is successd
    string parseInfoFromPKCS12(const string& passwd);


private:
    //!加载文件
    bool loadFile(const string& filePath, unsigned char* dest, unsigned int& len);
    //!释放
    void free_variable();
    //!对待发送文件签名
    bool signInfo(EVP_MD_CTX** mdctx);

private:
    PKCS12 *p12 {NULL};					//保存发送者私钥的PKCS12结构体指针
    X509 *RecvCert {NULL};				//保存接收者证书的X509结构体指针
    EVP_PKEY *pkey {NULL};				//保存发送者私钥的EVP_PKEY结构体指针
    X509 *SignCert {NULL};				//保存发送者证书的X509结构体指针
    unsigned char recvCertBuf[4096];	//保存接收者证书的数组
    unsigned int recvCertBufLen;		//接收者证书长度
    unsigned char PfxBuf[4096];			//保存发送者P12文件的数组
    unsigned int PfxBufLen;				//p12文件长度
    unsigned char Buffer[4096];			//保存待处理的文件数据的数组
    unsigned int BufferLen;				//数据长度
    unsigned char SessionKey[128];		//加密发送文件的会话密钥
    unsigned int SessionKeyLen;			//会话密钥长度
    unsigned char CipherSessionKey[256];//会话密钥的密文
    unsigned int CipherSessionKeyLen;	//会话密钥的密文长度
    unsigned char Digest[512];			//待发送文件的摘要值
    unsigned int DigestLen;				//摘要长度
    long unsigned int SignLen=0;        //签名值长度
    unsigned char Sign[512];            //签名值
    unsigned char SignCertBuf[4096];	//发送者证书数组
        unsigned int SignCertBufLen;		//发送者证书长度
        unsigned int DeSignLen=0;
        unsigned char DeSign[512];
    EVP_MD_CTX *mdctx {NULL};					//计算摘要的上下文
//    EVP_PKEY_CTX *Signctx;              //rsa签名上下文
    unsigned char *sig {NULL};                 //签名值
//    char err[1024];
};

#endif // ENDECRYINFO_H
