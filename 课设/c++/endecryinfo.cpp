#include "endecryinfo.h"
#include <memory.h>

EnDecryInfo::EnDecryInfo()
{

}

bool EnDecryInfo::loadReceLicense(const string &filePath)
{
    return loadFile(filePath,recvCertBuf, recvCertBufLen);
}

bool EnDecryInfo::loadSendLincense(const string &filePath)
{
    return loadFile(filePath,PfxBuf, PfxBufLen);
}

string EnDecryInfo::loadWaitSendFile(const string &filePath)
{
    char err[1024];

    //对待发送文件签名
    signInfo(&mdctx);

    memset(Buffer, 0, 4096);
    //打开待发送的文件
    FILE* fp = fopen(filePath.c_str(), "rb");
    if(fp == NULL) {
        free_variable();
        return "错误:文件打开失败";
    }
    //不断循环，以4096字节为单位读取文件，并摘要
    for(;;)
    {
        BufferLen=fread(Buffer, 1, 4096, fp);//每次读取4096个字节
        if(BufferLen <= 0) {
            break;
        }

        if(!EVP_SignUpdate(mdctx, Buffer, BufferLen)) {//摘要
            free_variable();
            fclose(fp);
            return "错误:签名摘要生成失败EVP_SignUpdate";
        }
    }
    fclose(fp);
    if(!EVP_DigestFinal(mdctx, Digest, &DigestLen))//完成摘要，获得摘要值
    {
        free_variable();
        ERR_error_string(ERR_get_error(),err);
        string strErr = err;
        string str ="错误:签名摘要生成摘要失败EVP_SignFinal " + strErr;
        return str;
    }

    //签名开始
    EVP_PKEY_CTX * Signctx = EVP_PKEY_CTX_new(pkey, NULL /* no engine */);
    if (!Signctx) {
        free_variable();
        return "错误: VP_PKEY_CTX_new Fail!";
    }

    if (EVP_PKEY_sign_init(Signctx) <= 0) {
        free_variable();
        return "错误: EVP_PKEY_sign_init Fail!";
    }

    if (EVP_PKEY_CTX_set_rsa_padding(Signctx, RSA_PKCS1_PADDING) <= 0) {
        free_variable();
        return "错误: EVP_PKEY_CTX_set_rsa_padding Fail!";
    }
    if (EVP_PKEY_CTX_set_signature_md(Signctx, EVP_sha256()) <= 0) {
        ERR_error_string(ERR_get_error(),err);
        printf("%s\n",err);
        string str = err;
        free_variable();
        return "错误: EVP_PKEY_CTX_set_signature_md Fail!" + str;
    }

    if (EVP_PKEY_sign(Signctx, NULL, &SignLen, Digest, DigestLen) <= 0) {
        free_variable();
        return "错误: VP_PKEY_sign Fail!";
    }

    sig = (unsigned char*) OPENSSL_malloc(SignLen);
    if (!sig) {
        free_variable();
        return "错误: OPENSSL_malloc Fail!";
    }

    if (EVP_PKEY_sign(Signctx, sig, &SignLen, Digest, DigestLen) <= 0) {
        free_variable();
        return "错误: EVP_PKEY_sign Fail!";
    }

    EVP_PKEY_free(pkey);
    pkey = NULL;
    X509_free(SignCert);
    SignCert = NULL;
    PKCS12_free(p12);
    p12 = NULL;
    return "";
}

string EnDecryInfo::parseInfoFromPKCS12(const string &passwd)
{
    if (!p12) {
        return "错误:未加载文件";
    }

    if(PKCS12_parse(p12, passwd.c_str(), &pkey, &SignCert,NULL) != 1)  {
        free_variable();
        return "错误:密码错误";
    }
    return "";
}

tuple<bool, string>  EnDecryInfo::encryptionFile(const string& infilePath, const string& outfilePath)
{
    //sign
    loadWaitSendFile(infilePath);

    memset(SessionKey, 0, 128);
    //产生随机数，作为会话密钥
    RAND_bytes(SessionKey, 128);
    CipherSessionKeyLen = EVP_PKEY_encrypt_old(CipherSessionKey, SessionKey, 128, X509_get_pubkey(RecvCert));

    if(CipherSessionKeyLen <= 0) {
        if (RecvCert) {
            X509_free(RecvCert);
        }

        return make_tuple(false, "错误: 会话密钥的密文长度为零");
    }

    if (RecvCert) {
        X509_free(RecvCert);
    }

    //利用会话密钥加密原文，并输出到密文到文件
    FILE *fpIn;
    FILE *fpOut;
    fpIn = fopen(infilePath.c_str(),"rb");
    if(fpIn == NULL) {
        return make_tuple(false, "ERR: 明文打开失败");
    }

    fpOut = fopen(outfilePath.c_str(),"wb");
    if(fpOut == NULL) {
        return make_tuple(false, "");
    }
    //密文文件格式：
    // |------------------|--------|------------------------|------------|--------------
    // |签名信息长度4Bytes|签名信息|会话密钥的密文长度4Bytes|会话密钥密文|原文数据的密文
    fwrite(&SignLen, 1, sizeof(SignLen), fpOut);//写签名长度到文件
    fwrite(sig, 1, SignLen, fpOut);			//写签名值到文件
    fwrite(&CipherSessionKeyLen, 1, sizeof(CipherSessionKeyLen), fpOut);	//写密文的会话密钥的长度到文件
    fwrite(CipherSessionKey, 1, CipherSessionKeyLen, fpOut);//写密文的会话密钥到文件
    OPENSSL_free(sig);
    sig = NULL;

    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    unsigned char out[1024];
    int outl;
    unsigned char in[1024];
    int inl;
    EVP_CIPHER_CTX_init(ctx);//初始化密码算法上下文
    //设置密码算法和密钥，这里采用128位的sm4算法。
    int rv = EVP_EncryptInit_ex(ctx,EVP_sm4_ecb(),NULL,SessionKey,NULL);
    if(rv != 1) {
        EVP_CIPHER_CTX_free(ctx);
        return make_tuple(false, "");
    }

    //以1024字节为单位，循环读取原文，并加密，然后输出到密文文件。
    for(;;) {
        inl = fread(in,1,1024,fpIn);//读取1024字节
        if(inl <= 0)
            break;
        rv = EVP_EncryptUpdate(ctx,out,&outl,in,inl);//加密
        if(rv != 1)
        {
            fclose(fpIn);
            fclose(fpOut);
            EVP_CIPHER_CTX_free(ctx);
            return make_tuple(false, "");
        }
        fwrite(out,1,outl,fpOut);//输出密文到文件
    }
    rv = EVP_EncryptFinal_ex(ctx,out,&outl);//完成加密，输出剩余的密文。
    if(rv != 1)
    {
        fclose(fpIn);
        fclose(fpOut);
        EVP_CIPHER_CTX_cleanup(ctx);
        return make_tuple(false, "");
    }
    fwrite(out,1,outl,fpOut);//写文件
    fclose(fpIn);
    fclose(fpOut);
    EVP_CIPHER_CTX_free(ctx);
    return make_tuple(true, "");
}

tuple<bool, string> EnDecryInfo::decryptionFile(const string &infilePath, const string &outfilePath)
{

    //根据安全报文的文件格式，读取安全报文
    //|------------------|--------|------------------------|------------|--------------
    //|签名信息长度4Bytes|签名信息|会话密钥的密文长度4Bytes|会话密钥密文|原文数据的密文
    //读取签名值，密文Sessionkey
    FILE *fp;
    fp = fopen(infilePath.c_str() ,"rb");
    if(fp == NULL) {
        free_variable();
        return make_tuple(false, "错误: 打开加密文件失败");
    }
    //读取签名值
    fread(&SignLen, 1, sizeof(SignLen), fp);

    //if((SignLen<=0)||(SignLen >256))
    if(SignLen <= 0) {
        fclose(fp);
        free_variable();
        return make_tuple(false, "错误: 签名错误！");
    }

    fread(Sign,1,SignLen,fp);
    //读取密文的会话密钥
    fread(&CipherSessionKeyLen,1,sizeof(CipherSessionKeyLen),fp);
    if((CipherSessionKeyLen<=0)||(CipherSessionKeyLen >256)) {
        fclose(fp);
        free_variable();
        return make_tuple(false, "错误: 读取会话密钥错误");
    }

    fread(CipherSessionKey,1,CipherSessionKeyLen,fp);
    //私钥接收者私钥解密会话密钥
    SessionKeyLen = EVP_PKEY_decrypt_old(SessionKey,CipherSessionKey,CipherSessionKeyLen,pkey);
    if(SessionKeyLen < 0)  {
        fclose(fp);
        free_variable();
        return make_tuple(false, "错误: 读取会话密钥错误");
    }

    //利用明文的会话密钥解密安全报文
    unsigned char out[1024+EVP_MAX_KEY_LENGTH];
    int outl;
    unsigned char in[1024];
    int inl;
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(ctx);
    FILE *fpOut;
    fpOut = fopen(outfilePath.c_str(), "wb");
    if(fpOut == NULL) {
        fclose(fp);
        free_variable();
        return make_tuple(false, "错误: 打开解密文件失败");
    }
    //设置解密算法和密钥。
    int rv = EVP_DecryptInit_ex(ctx,EVP_sm4_ecb(),NULL,SessionKey,NULL);
    if(rv != 1) {
        free_variable();
        fclose(fp);
        fclose(fpOut);
        EVP_CIPHER_CTX_cleanup(ctx);
        return make_tuple(false, "错误: 解密算法初始化失败");
    }
    //以1024字节为单位，循环读取安全报文，解密并保存到文件。
    for(;;)
    {
        inl = fread(in,1,1024,fp);//读取1024个字节
        if(inl <= 0) {
            break;
        }

        rv = EVP_DecryptUpdate(ctx,out,&outl,in,inl);//解密
        if(rv != 1) {
            fclose(fp);
            fclose(fpOut);
            EVP_CIPHER_CTX_cleanup(ctx);
            free_variable();
            return make_tuple(false, "错误: 解密失败");
        }
        fwrite(out,1,outl,fpOut);//保存到文件
    }
    rv = EVP_DecryptFinal_ex(ctx,out,&outl);//完成解密，输出剩余的明文。
    if(rv != 1)  {
        fclose(fp);
        fclose(fpOut);
        EVP_CIPHER_CTX_cleanup(ctx);
        free_variable();
        return make_tuple(false, "错误: 剩余明文输出失败");
    }

    fwrite(out,1,outl,fpOut);
    EVP_PKEY_free(pkey); pkey = NULL;
    X509_free(RecvCert); RecvCert = NULL;
    PKCS12_free(p12);    p12 = NULL;
    fclose(fp);
    fclose(fpOut);
    EVP_CIPHER_CTX_free(ctx);

    printf("进行验签\n");
    //解密完成，对原文验证签名
    fp = fopen(outfilePath.c_str(),"rb");
    if(fp == NULL) {
        X509_free(SignCert);
        return make_tuple(false, "错误: 打开验签文件失败！");
    }
    printf("摘要上下文初始化\n");
    mdctx = EVP_MD_CTX_new();
    EVP_MD_CTX_init(mdctx);	//初始化摘要上下文
    if(!EVP_SignInit_ex(mdctx, EVP_sha256(), NULL))	//设置摘要算法，这里选择SM3
    {
        X509_free(SignCert);
        fclose(fp);
        return make_tuple(false, "错误: 初始化签名失败！");
    }
    printf("摘要生成中\n");
    //不断循环，以4096字节为单位读取文件，并摘要
    for(;;)
    {
        BufferLen=fread(Buffer,1,4096,fp);//每次读取4096个字节
        if(BufferLen <=0)
            break;
        if(!EVP_SignUpdate(mdctx, Buffer, BufferLen))//摘要
        {
            X509_free(SignCert);
            fclose(fp);
            return make_tuple(false, "错误: 签名摘要生成失败EVP_SignUpdate");
        }
    }
    fclose(fp);
    if(!EVP_DigestFinal(mdctx,Digest,&DigestLen))//完成签名，获得摘要
    {
        char err[1024];
        X509_free(SignCert);
        ERR_error_string(ERR_get_error(),err);
        string str = err;
        return make_tuple(false, "错误: 签名摘要生成摘要失败EVP_SignFinal" + str);
    }

    return make_tuple(true, "");
}

string EnDecryInfo::checkSign()
{
    char err[1024];
    int ret = 0;
    EVP_PKEY_CTX *Verifyctx = EVP_PKEY_CTX_new(X509_get_pubkey(SignCert), NULL /* no engine */);

    if (!Verifyctx) {
        X509_free(SignCert);
        return "错误: EVP_PKEY_CTX_new Fail!";
    }

    if (EVP_PKEY_verify_init(Verifyctx) <= 0) {
        X509_free(SignCert);
        return "错误: EVP_PKEY_verify_init Fail!";
    }

    if (EVP_PKEY_CTX_set_rsa_padding(Verifyctx, RSA_PKCS1_PADDING) <= 0) {
        X509_free(SignCert);
        return "错误: EVP_PKEY_CTX_set_rsa_padding Fail!";
    }

    if (EVP_PKEY_CTX_set_signature_md(Verifyctx, EVP_sha256()) <= 0) {
        ERR_error_string(ERR_get_error(), err);
        printf("%s\n",err);
        string str = err;
        X509_free(SignCert);
        return "错误: EVP_PKEY_CTX_set_signature_md Fail!" + str;
    }

    if((ret = EVP_PKEY_verify(Verifyctx, Sign, SignLen, Digest, DigestLen))== 1)
    {
        X509_free(SignCert);
        return "验签成功，可信!";
    }
    else if(ret == 0)  {
        X509_free(SignCert);
        return "错误: 验签失败，不可信!";
    }
    else {
        X509_free(SignCert);
        return "错误: 验签失败，错误!";
    }

    return "";
}

string EnDecryInfo::checkPasswd_encry(const string &passwd, const string &sendFile, const string &receFile)
{
    //打开接收者证书
    FILE* fp = fopen(receFile.c_str() ,"rb");
    if(fp==NULL)
    {
        return 0;
        string file = receFile.c_str();
        return "ERR: File to open" + file;
    }
    recvCertBufLen = fread(recvCertBuf,1,4096,fp);
    fclose(fp);
    //打开发送者PFX（p12）文件
    fp = fopen(sendFile.c_str(),"rb");
    if(fp==NULL)
    {
        string file = sendFile.c_str();
        return "ERR: File to open" + file;
    }
    PfxBufLen = fread(PfxBuf,1,4096,fp);
    fclose(fp);
    //把接收者证书转化为X509结构体
    unsigned char *tmp = recvCertBuf;
    RecvCert = d2i_X509(NULL,(const unsigned char **)&tmp, recvCertBufLen);
    if(RecvCert == NULL) {
        return "ERR:File to transform Err";
    }
    //把P12文件转化为PKCS12结构体
    BIO* bio = BIO_new(BIO_s_mem());
    BIO_write(bio,PfxBuf,PfxBufLen);
    p12 = d2i_PKCS12_bio(bio, NULL);
    if(p12 == NULL)  {
        free_variable();
        BIO_free_all(bio);
        fprintf(stderr,"File to transform P12 \n");
        return "ERR:File to transform P12";
    }
    BIO_free_all(bio);
    //从PKCS12结构体中解析获得私钥和证书
    auto res = parseInfoFromPKCS12(passwd);
    if (res != "") {
        return res;
    }
    return "密码验证成功";
}

string EnDecryInfo::checkPasswd_decry(const string &passwd, const string &sendFile, const string &receFile)
{
    OpenSSL_add_all_algorithms();
    //打开发送者证书
    FILE *fp;
    fp = fopen(sendFile.c_str(),"rb");
    if(fp==NULL)
    {
        fprintf(stderr,"打开发送者证书失败！\n");
        return 0;
    }
    SignCertBufLen = fread(SignCertBuf, 1, 4096, fp);
    fclose(fp);

    //转化为X509结构体
    unsigned char *tmp = SignCertBuf;
    SignCert = d2i_X509(NULL,(const unsigned char **)&tmp,SignCertBufLen);
    if(SignCert == NULL)
    {
        printf("证书转换为x509结构体失败！\n");
        return 0;
    }

    //打开接收者p12文件
    fp = fopen(receFile.c_str(),"rb");
    if(fp==NULL)
    {
        fprintf(stderr,"p12证书打开失败\n");
        return 0;
    }
    PfxBufLen = fread(PfxBuf,1,4096,fp);
    fclose(fp);
    //转化为PKCS12结构体
    BIO* bio = BIO_new(BIO_s_mem());
    BIO_write(bio,PfxBuf,PfxBufLen);
    p12 = d2i_PKCS12_bio(bio, NULL);
    if(p12 ==NULL)
    {
        free_variable();
        return "ERR:p12证书转换失败！";
    }
    BIO_free_all(bio);

    //解析PKCS12，获取接收者的私钥和证书
    int rv = PKCS12_parse(p12, passwd.c_str(),&pkey,&RecvCert,NULL);
    if(rv != 1)
    {
        free_variable();
        return "ERR:密码错误";
    }
    return "密码验证成功";
}

bool EnDecryInfo::signInfo(EVP_MD_CTX** mdctx)
{
    //对待发送文件签名
    *mdctx = EVP_MD_CTX_new();
    EVP_MD_CTX_init(*mdctx);	//初始化摘要上下文
    if(!EVP_SignInit_ex(*mdctx, EVP_sha256(), NULL))	//设置摘要算法，这里选择SM3
    {
        free_variable();
        printf("初始化摘要失败！\n");
        return false;
    }
    return true;
}

bool EnDecryInfo::loadFile(const string &filePath, unsigned char *dest, unsigned int &len)
{
    FILE * fp = fopen(filePath.c_str(),"rb");
    if(fp == NULL) {
        fprintf(stderr,"File to open %s\n", filePath.c_str());
        return false;
    }

    len = fread(dest,1,4096,fp);
    fclose(fp);
    return true;
}



void EnDecryInfo::free_variable()
{
    if (RecvCert) {
        X509_free(RecvCert);
        RecvCert = NULL;
    }

    if (p12) {
        PKCS12_free(p12);
        p12 = NULL;
    }

    if (SignCert) {
        X509_free(SignCert);
        SignCert = NULL;
    }

    if (pkey) {
        EVP_PKEY_free(pkey);
        pkey = NULL;
    }

    if (sig) {
        OPENSSL_free(sig);
        sig = NULL;
    }

}


