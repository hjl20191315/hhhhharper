/**************************************************
函数名:RecvSecMsg
函数功能：接收安全报文。
参数说明：
sSendCert：[IN] ,发送方的数字证书
sRecvPfx：[IN] ,接收方的私钥文件
sPass：	[IN] ,接收方的私钥保护口令
sInFile：[IN] ,安全报文文件路径
sOutFile：[IN] ,输出的原文文件路径
处理过程：假设B接收A发过来的安全报文
1）B使用自己的私钥解密会话密钥。
2）B使用会话密钥解密密文，得到明文
3）B用A的证书验证A的签名，确认是A发送的数据。
安全报文的格式：
|------------------|--------|------------------------|------------|--------------
|签名信息长度4Bytes|签名信息|会话密钥的密文长度4Bytes|会话密钥密文|原文数据的密文
***************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/err.h>
#include <openssl/objects.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/pkcs12.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>


#define FALSE 0
#define TRUE 1
int main(int argc, char *argv[])
{
	BIO *bio;
	int rv;
	PKCS12 *p12 = NULL;					//接收者p12结构体指针
	X509 *RecvCert = NULL;				//接收者X509结构体指针
	EVP_PKEY *pkey = NULL;				//接收者私钥结构体指针
	X509 *SignCert = NULL;				//发送者（签名者）X509结构体指针
	unsigned char *tmp;
	unsigned char SignCertBuf[4096];	//发送者证书数组
	unsigned int SignCertBufLen;		//发送者证书长度
	unsigned char PfxBuf[4096];			//接收者p12数据数组
	unsigned int PfxBufLen;				//p12数据长度
	unsigned char Buffer[4096];			//数据缓冲区数组
	unsigned int BufferLen;				//数据长度
	unsigned char SessionKey[128];		//会话密钥
	unsigned int SessionKeyLen;			//会话密钥长度
	unsigned char CipherSessionKey[256];//密文的会话密钥
	unsigned int CipherSessionKeyLen=0;	//密文的会话密钥的长度
	unsigned char Sign[512];	//签名值
	unsigned char Digest[512];
	unsigned int DigestLen=0;
	long unsigned int SignLen=0;				//签名值长度
	unsigned int DeSignLen=0;
	unsigned char DeSign[512];
	int ret;                        //验签返回值
	EVP_PKEY_CTX *Verifyctx;                   //验签上下文
	EVP_MD_CTX *mdctx;					//摘要上下文
	char err[1024];

	OpenSSL_add_all_algorithms();
	//打开发送者证书
	FILE *fp;
	fp = fopen(argv[1],"rb");
	if(fp==NULL)
    {
        fprintf(stderr,"打开发送者证书失败！\n");
        return 0;
    }
	SignCertBufLen = fread(SignCertBuf,1,4096,fp);
	fclose(fp);

	//转化为X509结构体
	tmp = SignCertBuf;
	SignCert = d2i_X509(NULL,(const unsigned char **)&tmp,SignCertBufLen);
	if(SignCert == NULL)
	{
	    printf("证书转换为x509结构体失败！\n");
		return 0;
	}

	//打开接收者p12文件
	fp = fopen(argv[2],"rb");
	if(fp==NULL)
    {
        fprintf(stderr,"p12证书打开失败\n");
        return 0;
    }
	PfxBufLen = fread(PfxBuf,1,4096,fp);
	fclose(fp);
	//转化为PKCS12结构体
	bio = BIO_new(BIO_s_mem());
	rv = BIO_write(bio,PfxBuf,PfxBufLen);
	p12 = d2i_PKCS12_bio(bio, NULL);
	if(p12 ==NULL)
	{
	    fprintf(stderr,"p12证书转换失败！\n");
		X509_free(SignCert);
		BIO_free_all(bio);
		return 0;
	}
	BIO_free_all(bio);

	//解析PKCS12，获取接收者的私钥和证书
	char Passwd[1024];
	printf("请输入p12证书密码\n");
	scanf("%s",Passwd);
	rv = PKCS12_parse(p12, Passwd,&pkey,&RecvCert,NULL);
	if(rv != 1)
	{
	    fprintf(stderr,"密码错误\n");
		X509_free(SignCert);
		PKCS12_free(p12);
		return 0;
	}

	//根据安全报文的文件格式，读取安全报文
	//|------------------|--------|------------------------|------------|--------------
	//|签名信息长度4Bytes|签名信息|会话密钥的密文长度4Bytes|会话密钥密文|原文数据的密文
	//读取签名值，密文Sessionkey
	fp = fopen(argv[3],"rb");
	if(fp == NULL)
	{
	    fprintf(stderr,"打开加密文件失败\n");
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		return 0;
	}
	//读取签名值
	fread(&SignLen,1,sizeof(SignLen),fp);

	//if((SignLen<=0)||(SignLen >256))
	if(SignLen<=0)
	{
	    fprintf(stderr,"签名错误！");
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		fclose(fp);
		return 0;
	}

	fread(Sign,1,SignLen,fp);
	//读取密文的会话密钥
	fread(&CipherSessionKeyLen,1,sizeof(CipherSessionKeyLen),fp);
	if((CipherSessionKeyLen<=0)||(CipherSessionKeyLen >256))
	{
	    fprintf(stderr,"读取会话密钥错误!");
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		fclose(fp);
		return 0;
	}

	fread(CipherSessionKey,1,CipherSessionKeyLen,fp);
	//私钥接收者私钥解密会话密钥
	SessionKeyLen = EVP_PKEY_decrypt_old(SessionKey,CipherSessionKey,CipherSessionKeyLen,pkey);
	if(SessionKeyLen < 0)
	{
	    fprintf(stderr,"会话密钥解密错误!");

		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		fclose(fp);
		return 0;
	}

	//利用明文的会话密钥解密安全报文
	unsigned char out[1024+EVP_MAX_KEY_LENGTH];
	int outl;
	unsigned char in[1024];
	int inl;
	EVP_CIPHER_CTX *ctx;
	ctx = EVP_CIPHER_CTX_new();
	EVP_CIPHER_CTX_init(ctx);
	FILE *fpOut;
	fpOut = fopen(argv[4],"wb");
	if(fpOut==NULL)
	{
	    fprintf(stderr,"打开解密文件失败!");
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		fclose(fp);
		return 0;
	}
	//设置解密算法和密钥。
	rv = EVP_DecryptInit_ex(ctx,EVP_sm4_ecb(),NULL,SessionKey,NULL);
	if(rv != 1)
	{
	    fprintf(stderr,"解密算法初始化失败!");
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		fclose(fp);
		fclose(fpOut);
		EVP_CIPHER_CTX_cleanup(ctx);
		return 0;
	}
	//以1024字节为单位，循环读取安全报文，解密并保存到文件。
	for(;;)
	{
		inl = fread(in,1,1024,fp);//读取1024个字节
		if(inl <= 0)
			break;
		rv = EVP_DecryptUpdate(ctx,out,&outl,in,inl);//解密
		if(rv != 1)
		{
			EVP_PKEY_free(pkey);
			X509_free(SignCert);
			X509_free(RecvCert);
			PKCS12_free(p12);
			fclose(fp);
			fclose(fpOut);
			EVP_CIPHER_CTX_cleanup(ctx);
			fprintf(stderr,"解密失败");
			return 0;
		}
		fwrite(out,1,outl,fpOut);//保存到文件
	}
	rv = EVP_DecryptFinal_ex(ctx,out,&outl);//完成解密，输出剩余的明文。
	if(rv != 1)
	{
	    fprintf(stderr,"剩余明文输出失败!");
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		fclose(fp);
		fclose(fpOut);
		EVP_CIPHER_CTX_cleanup(ctx);
		return 0;
	}
	fwrite(out,1,outl,fpOut);
	EVP_PKEY_free(pkey);
	X509_free(RecvCert);
	PKCS12_free(p12);
	fclose(fp);
	fclose(fpOut);
	EVP_CIPHER_CTX_free(ctx);

	printf("进行验签\n");
	//解密完成，对原文验证签名
 	fp = fopen(argv[4],"rb");
	if(fp==NULL)
	{
	    fprintf(stderr,"打开验签文件失败！");
		X509_free(SignCert);
		return 0;
	}
	printf("摘要上下文初始化\n");
	mdctx = EVP_MD_CTX_new();
	EVP_MD_CTX_init(mdctx);	//初始化摘要上下文
	if(!EVP_SignInit_ex(mdctx, EVP_sha256(), NULL))	//设置摘要算法，这里选择SM3
	{
		X509_free(SignCert);
		fprintf(stderr,"初始化签名失败！\n");
		fclose(fp);
		return 0;
	}
	printf("摘要生成中\n");
	//不断循环，以4096字节为单位读取文件，并摘要
	for(;;)
	{
		BufferLen=fread(Buffer,1,4096,fp);//每次读取4096个字节
		if(BufferLen <=0)
			break;
		if(!EVP_SignUpdate(mdctx, Buffer, BufferLen))//摘要
		{
			X509_free(SignCert);
			fclose(fp);
			fprintf(stderr,"签名摘要生成失败EVP_SignUpdate\n");
			return 0;
		}
	}
	fclose(fp);
	if(!EVP_DigestFinal(mdctx,Digest,&DigestLen))//完成签名，获得摘要
	{
		X509_free(SignCert);
		ERR_error_string(ERR_get_error(),err);
		printf("签名摘要生成摘要失败EVP_SignFinal %s\n",err);
		return 0;
	}

	printf("开始解签\n");
	/*
	DeSignLen = EVP_PKEY_decrypt_old(DeSign,Sign,SignLen,X509_get_pubkey(SignCert));
	ERR_error_string(ERR_get_error(),err);
    printf("%s\n",err);
	printf("DeSignLen:%d",DeSignLen);
	if(DeSignLen <= 0)
	{
	    fprintf(stderr,"解签错误!");
		EVP_PKEY_free(pkey);
		X509_free(SignCert);
		X509_free(RecvCert);
		PKCS12_free(p12);
		fclose(fp);
		return 0;
	}

	printf("解签为%s\n",DeSign);
	if(strcmp(DeSign,Digest) == 0)
    {
        fprintf(stdin,"签名验证成功！");
        X509_free(SignCert);
        EVP_MD_CTX_free(mdctx);
        return TRUE;
    }
    else
    {
        fprintf(stdin,"签名验证失败！");
        X509_free(SignCert);
        EVP_MD_CTX_free(mdctx);
        return FALSE;
    }

*/
    Verifyctx = EVP_PKEY_CTX_new(X509_get_pubkey(SignCert), NULL /* no engine */);
     if (!Verifyctx)
      {
          fprintf(stderr,"EVP_PKEY_CTX_new Fail!");
          X509_free(SignCert);
		  return 0;
      }
      if (EVP_PKEY_verify_init(Verifyctx) <= 0)
      {
          fprintf(stderr,"EVP_PKEY_verify_init Fail!");
          X509_free(SignCert);
		  return 0;
      }
      if (EVP_PKEY_CTX_set_rsa_padding(Verifyctx, RSA_PKCS1_PADDING) <= 0)
      {
          fprintf(stderr,"EVP_PKEY_CTX_set_rsa_padding Fail!");
          X509_free(SignCert);
		  return 0;
      }
       if (EVP_PKEY_CTX_set_signature_md(Verifyctx, EVP_sha256()) <= 0)
       {
           fprintf(stderr,"EVP_PKEY_CTX_set_signature_md Fail!");
           ERR_error_string(ERR_get_error(),err);
           printf("%s\n",err);
          X509_free(SignCert);
		  return 0;
       }
       if((ret = EVP_PKEY_verify(Verifyctx, Sign, SignLen, Digest,DigestLen) )== 1)
       {
           printf("验签成功，可信！\n");
          X509_free(SignCert);
		  return 1;

       }
       else if(ret = 0)
       {
           printf("验签失败，不可信！\n");
          X509_free(SignCert);
		  return 0;
       }
       else
       {
           printf("验签失败，错误\n");
          X509_free(SignCert);
		  return 0;
       }

}
