#ifndef __SDF_H
#define __SDF_H

#define TRUE 0x1
#define FALSE 0x0
//openssldefine
#include <openssl/err.h>
#include <openssl/objects.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/pkcs12.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>
#include <openssl/crypto.h>
#include <string.h>
#include <stdlib.h>


// Error Code
#define SDF_OK 0x0 //操作成功
#define SDR_BASE   0x01000000   //错误码基础值
#define SDR_UNKNOWERR   SDR_BASE+0x00000001  //未知错误
#define SDR_NOTSUPPORT   SDR_BASE+0x00000002  //不支持的接口调用
#define SDR_COMMFAIL   SDR_BASE +0x00000003  //与设备通信失败
#define SDR_HARDFAIL    SDR_BASE+ 0x00000004   //运算模块无响应
#define SDR_OPENDEVICE   SDR_BASE+0x00000005  //打开设备失败
#define SDR_OPENSESSION  SDR_BASE + 0x00000006  //创建会话失败
#define SDR_PARDENY   SDR_BASE +0x00000007    //无私钥使用权限
#define SDR_KEYNOTEXIST   SDR_ BASE+0x00000008   //不存在的密钥调用
#define SDR_ALGNOTSUPPORT   SDR_BASE + 0x00000009  //不支持的算法调用
#define SDR_ALGMODNOTSUPPORT   SDR_BASE+ 0x0000000A   //不支持的算法模式调用
#define SDR_PKOPERR   SDR_BASE+ 0x0000000B   //公钥运算失败
#define SDR_SK OPERR  SDR_BASE + 0x0000000C  //私钥运算失败
#define SDR_SIGNERR    SDR _BASE+0x0000000D   //签名运算失败
#define SDR_VERIFYERR   SDR_BASE +0x0000000E   //验证签名失败
#define SDR_SYMOPERR   SDR_BASE+ 0x0000000F   //对称算法运算失败
#define SDR_STEPERR   SDR_BASE+0x00000010  //多步运算步骤锗误
#define SDR_FILES1ZEERR   SDR_BASE+0x00000011  //文件长度超出限制
#define SDR_FILENOEXIST   SDR_BASE+0x00000012   //指定的文件不存在
#define SDR_FILEOFSERR  SDR_BASE+0x00000013  //文件起始位置错误
#define SDR_KEYTYPEERR  SDR_BASE+0x00000014  //密钥类型错误
#define SDR_KEYERR  SDR_BASE+0x00000015  //密钥错误
#define SDR_ENCDATAERR  SDR_BA3E+0x00000016  //ECC加密数据错误
#define SDR_RANDERR  SDR_BASE+0x00000017  //随机数产生失败
#define SDR_PRKRERR  SDR_BASE+0x00000018  //私钥使用权限获取失败
#define SDR_MACFRR  SDR_BASE+0x00000019 //MAC运算失败
#define SDR_FILEEXISTS   SDR_BASE+ 0x0000001A  //指定文件已存在
#define SDR_FILEWERR  SDR_BASE+0x0000001B  //文件写入失败
#define SDR_NORUFFER  SDR_BASE+0x0000001c  //存储空间不足
#define SDR_INARGERR  SDR_BASE+0x0000001D  //输入参数错误
#define SDR_OUTARGERR  SDR_BASE +0x0000001E  //输出参数错误

//********************************
//密码参数
//********************************
#define MAX_IV_LEN						32	//初始化向量的最大长度
#define	MAX_FILE_NAME_LEN				32	//文件名最大长度
//#define MAX_CONTAINER_NAME_LEN			128	//容器名最大长度
#define MIN_PIN_LEN						6	//最小的PIN长度

#define MAX_RSA_MODULUS_LEN				256	//RSA算法模数的最大长度
#define MAX_RSA_EXPONENT_LEN			4	//RSA算法指数的最大长度

#define ECC_MAX_XCOORDINATE_BITS_LEN	512	//ECC算法X坐标的最大长度
#define ECC_MAX_YCOORDINATE_BITS_LEN	512	//ECC算法Y坐标的最大长度
#define ECC_MAX_MODULUS_BITS_LEN		512	//ECC算法模数的最大长度


#define SGD_SM1_ECB				0x00000101	//SM1算法ECB加密模式
#define SGD_SM1_CBC				0x00000102	//SM1算法CBC加密模式
#define SGD_SM1_CFB				0x00000104	//SM1算法CFB加密模式
#define SGD_SM1_OFB				0x00000108	//SM1算法OFB加密模式
#define SGD_SM1_MAC				0x00000110	//SM1算法MAC运算
#define SGD_SSF33_ECB			0x00000201	//SSF33算法ECB加密模式
#define SGD_SSF33_CBC			0x00000202	//SSF33算法CBC加密模式
#define SGD_SSF33_CFB			0x00000204	//SSF33算法CFB加密模式
#define SGD_SSF33_OFB			0x00000208	//SSF33算法OFB加密模式
#define SGD_SSF33_MAC			0x00000210	//SSF33算法MAC运算
#define SGD_SM4_ECB				0x00000401	//SMS4算法ECB加密模式
#define SGD_SM4_CBC				0x00000402	//SMS4算法CBC加密模式
#define SGD_SM4_CFB				0x00000404	//SMS4算法CFB加密模式
#define SGD_SM4_OFB				0x00000408	//SMS4算法OFB加密模式
#define SGD_SM4_MAC				0x00000410	//SMS4算法MAC运算

//非对称密码算法标识
#define SGD_RSA					0x00010000	//RSA算法
#define SGD_SM2_1				0x00020100	//椭圆曲线签名算法
#define SGD_SM2_2				0x00020200	//椭圆曲线密钥交换协议
#define SGD_SM2_3				0x00020400	//椭圆曲线加密算法

//密码杂凑算法标识
#define SGD_SM3					0x00000001	//SM3杂凑算法
#define SGD_SHA1				0x00000002	//SHA1杂凑算法
#define SGD_SHA256				0x00000004	//SHA256杂凑算法

#define ECCref_MAX_BITS			512
#define ECCref_MAX_LEN			((ECCref_MAX_BITS+7) /8)

//定义设备信息结构
typedef struct DeviceInfo_st{
	unsigned char IssuerName[40]; //设备生产厂商名称
	unsigned char DeviceName[16];
	unsigned char DeviceSerial[16];
	unsigned int DeviceVersion;
	unsigned int StandardVersion;
	unsigned int AsymAlgAbility[2];
	unsigned int SymAlgAbilty;
	unsigned int HashAlgAbility;
	unsigned int BufferSize;
}DEVICEINFO;

typedef struct ECCrefPublicKey_st{
	unsigned int bits;						//密钥位长
	unsigned char x[ECCref_MAX_LEN];		//x分量
	unsigned char y[ECCref_MAX_LEN];		//y分量
}ECCrefPublicKey;

typedef struct ECCrefPrivateKey_st{
	unsigned int bits;						//密钥位长
	unsigned char K[ECCref_MAX_LEN];		//私钥
}ECCrefPrivateKey;

typedef struct DeviceHandle_st{
	unsigned int DeviceState;//未就绪
	unsigned int VerifyPin;//未验证
}DeviceHandle;
//********************************
//设备管理
//********************************

/*
功能：打开密码设备。
参数∶
phDeviceHandle[out] 返回设备句柄

返回值∶
   0   成功
  非0  失败，返回错误代码
*/
int SDF_OpenDevice(void ** phDeviceHandle);

/*
功能∶关闭密码设备，并释放相关资源。
参数∶
hDeviceHandle[in] 已打开的设备句柄
返回值∶
	0（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_CloseDevice(void *hDeviceHandle);

/*


功能∶获取密码设备能力描述。;
参数∶
hSesionHandle[in]与设备建立的会话句柄
pstDevceInfo [out]设备能力描述信息，内容及格式见设备信息定义
返回值∶
	0（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_GetDeviceInfo( void * hSessionHandle,
                       DEVICEINFO * pstDeviceInfo);



/*
功能：获取指定长度的随机数
参数：
 uiLength[in]  欲获取的随机数长度
 pucRandom[ out] 缓冲区指针，用于存放获取的随机数
 返回值∶
	 00（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_GenerateRandom (void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom);

#endif
