#include "sdf.h"
static int getRandom(char *r, int length);
int SDF_GenerateRandom (void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom)
{
	getRandom(pucRandom,uiLength);
	return SDF_OK;
}
static int getRandom(char *r, int length)
{
    RAND_bytes(r, length);
    return SDF_OK;
}
