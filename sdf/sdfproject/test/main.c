#include<stdio.h>
#include<stdlib.h>
#include "sdf.h"
#include <time.h>
#include <stdio.h>


int main(){
   void **pdh;
   pdh=(void **)malloc(20);
   int ret;
   ret = SDF_OpenDevice(pdh);
   if(ret != SDR_OK)
   {
	   printf("打开设备失败\n");
   }
   else
   {
	   printf("打开设备成功！\n");
	 }
   printf("查看设备信息\n");
   DEVICEINFO a;
   ret = SDF_GetDeviceInfo(*pdh,&a);
   if(ret !=SDR_OK)
           printf("查看设备信息失败！\n");
   else
           printf("查看设备信息成功！\n");
   printf("设备名字叫做%s\n",a.DeviceName);
   printf("设备版本号为%d\n",a.DeviceVersion);
   printf("想要获取的随机数长度为：\n");
   int n;
   scanf("%d",&n);
   char string[100];
   ret = SDF_GenerateRandom(*pdh,n,string);
   if(ret !=SDR_OK)
	   printf("生成随机数失败！");
    else
	    printf("生成的随机数为%s\n",string);
   
   //对密钥容器生成两对密钥
   KEYBOX Keybox;
   Keybox.length=2;
  
   /*生成加解密密钥对*/
  
   printf("*************************生成加解密密钥对***********************************\n");
   ret =  SDF_GenerateKeyPair_ECC(*pdh,SGD_SM2_3,256,&(Keybox.mysm2_key_pair[0].pub_key),&(Keybox.mysm2_key_pair[0].pri_key));//我设的生成密钥算法默认长度32字节,256bit；
   if(ret ==SDR_OK)
   {
	   printf("生成加解密密钥对成功!\n");
           /*
	   printf("加密公钥的位数：%d\n",(Keybox.mysm2_key_pair[0].pub_key).bits);
	   printf("加密公钥的x坐标：%s\n",(Keybox.mysm2_key_pair[0].pub_key).x);
	   printf("加密公钥的y坐标：%s\n",(Keybox.mysm2_key_pair[0].pub_key).y);
	   printf("加密私钥的位数:%d\n",(Keybox.mysm2_key_pair[0].pri_key).bits);
	   printf("加密私钥为：%s\n",(Keybox.mysm2_key_pair[0].pri_key).K);
           */
           
   }
   else
   {
	   printf("生成加解密密钥失败！");
   }
   /*生成签名验签密钥对*/
   printf("*************************生成签名验签密钥对***********************************\n");
   ret =  SDF_GenerateKeyPair_ECC(*pdh,SGD_SM2_1,256,&(Keybox.mysm2_key_pair[1].pub_key),&(Keybox.mysm2_key_pair[1].pri_key));
    if(ret ==SDR_OK)
   {
           printf("生成签名验签密钥对成功!\n");
           /*
           printf("签名公钥的位数：%d\n",(Keybox.spub_key).bits);
           printf("签名公钥的x坐标：%s\n",(Keybox.spub_key).x);
           printf("签名公钥的y坐标：%s\n",(Keybox.spub_key).y);
           printf("签名私钥的位数:%d\n",(Keybox.spri_key).bits);
           printf("签名私钥为：%s\n",(Keybox.spri_key).K);
           */
   }
   else
   {
           printf("生成签名验签密钥失败！");
   }

    FILE *fp;  // 文件指针
    unsigned int uiKeyIndex;
    unsigned int bits;
    unsigned char x[ECCref_MAX_LEN];
    unsigned char y[ECCref_MAX_LEN];
    //unsigned int uiKeyIndex1 = SGD_PUBLIC_KEY_ENCRYPT;
    //unsigned int bits1=201;
    //unsigned char x1[ECCref_MAX_LEN]="ABCD12345";
    //unsigned char y1[ECCref_MAX_LEN]="YUIO87651";
    // 判断文件是否能够正确创建/打开
    if( (fp=fopen(FILE_PATH,"wt+")) == NULL ){
        perror(FILE_PATH);
        exit(1);
    }
   
  
    printf("*************************将两对密钥放进密钥容器***************************\n");

    fprintf(fp,"%d\t%d\t%s\t%s\n", SGD_PUBLIC_KEY_ENCRYPT, (Keybox.mysm2_key_pair[0].pub_key).bits, (Keybox.mysm2_key_pair[0].pub_key).x, (Keybox.mysm2_key_pair[0].pub_key).y);//将加密公钥写入文件
    fprintf(fp,"%d\t%d\t%s\t%s\n", SGD_PRIVATE_KEY_ENCRYPT, (Keybox.mysm2_key_pair[0].pri_key).bits, (Keybox.mysm2_key_pair[0].pri_key).K, "空");//将加密私钥入文件
    fprintf(fp,"%d\t%d\t%s\t%s\n", SGD_PUBLIC_KEY_SIGN, (Keybox.mysm2_key_pair[1].pub_key).bits, (Keybox.mysm2_key_pair[1].pub_key).x, (Keybox.mysm2_key_pair[1].pub_key).y);//将签名公钥写入文件
    fprintf(fp,"%d\t%d\t%s\t%s\n", SGD_PRIVATE_KEY_SIGN, (Keybox.mysm2_key_pair[1].pri_key).bits, (Keybox.mysm2_key_pair[1].pri_key).K, "空");//将签名私钥写入文件
    
    // 刷新缓冲区，将缓冲区的内容写入文件
    fflush(fp);
    
    /*// 重置文件内部位置指针，让位置指针指向文件开头(很重要！）
    rewind(fp);
    */
    printf("两对密钥已成功放入密钥容器(demo.txt)~\n");

 /*   // 从文件中读取加密公钥和签名公钥的信息
    printf("\n读取密钥容器（文件）中的加密公钥和签名公钥内容：\n");
    while(fscanf(fp, "%d\t%d\t%s\t%s", &uiKeyIndex, &bits, x, y) != EOF){ //fscanf是从文件中读取内容的啦
       // 挑选一下，我想要加密公钥和签名公钥
        if(uiKeyIndex == SGD_PUBLIC_KEY_ENCRYPT || uiKeyIndex == SGD_PUBLIC_KEY_SIGN)
        printf("%d\t%d\t%s\t%s\n", uiKeyIndex, bits, x, y); //打印到显示器
    } */
    fclose(fp);//关闭文件


   
   printf("*************************从密钥容器中导出签名公钥和加密公钥************************\n");
   
   ECCrefPublicKey key;
   //导出ECC签名公钥
   ret = SDF_ExportSignPublicKey_ECC(*pdh,SGD_PUBLIC_KEY_SIGN,&key);
    if(ret !=SDR_OK)
   {
	   printf("导出签名公钥失败!"); 
   }
    else
   {
         printf("导出签名公钥成功！\n");
	 printf("签名公钥的长度为%d\n",key.bits);
	 printf("签名公钥的x坐标为%s\n",key.x);
	 printf("签名公钥的y坐标为%s\n",key.y);

   }


    ECCrefPublicKey key1;
   //导出ECC加密公钥
    ret = SDF_ExportEncPublicKey_ECC(*pdh,SGD_PUBLIC_KEY_ENCRYPT,&key1);
   if(ret !=SDR_OK)
   {
           printf("导出加密公钥失败!");
   }
    else
   {
         printf("导出加密公钥成功！\n");
         printf("加密公钥的长度为%d\n",key1.bits);
         printf("加密公钥的x坐标为%s\n",key1.x);
         printf("加密公钥的y坐标为%s\n",key1.y);

   }
   
   
   ret = SDF_CloseDevice(*pdh);
   if(ret != SDR_OK)
   {
	   printf("关闭不成功！\n");
   }
   else
   {
	   printf("关闭成功！\n");
   }
   free(pdh);

}
