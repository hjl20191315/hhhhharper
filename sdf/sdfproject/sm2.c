#include<string.h>
#include<openssl/obj_mac.h>
#include<openssl/bn.h>
#include<openssl/ec.h>

int main()
{
	
	BN_CTX* ctx = NULL;    //存储大数运算得零时变量
	BIGNUM* bn_d = NULL,* bn_x=NULL,* bn_y=NULL;//bn_d?? ,坐标x,y
	const BIGNUM* bn_order; //阶
	EC_GROUP* group = NULL; //椭圆曲线
	EC_POINT* ec_pt = NULL;//基准坐标点
	unsigned char pub_key_x[33], pub_key_y[33];

	

	if (!(ctx = BN_CTX_secure_new())) //初始化一个BIGNUM结构体
	{                         //BN_CTX_secure_new()动态分配并初始化一个 BIGNUM 结构体。如果执行成功，
		                          //返回值为指向 BIGNUM 结构体的指针，这个 BIGNUM 的值被设为 0。如果执行失败则返回 NULL 。
		goto clear_up;
	}
	BN_CTX_start(ctx);        //如果要获取 BN_CTX 结构体中的临时 BIGNUM 变量，方法如下：
	bn_d = BN_CTX_get(ctx);   //先调用 BN_CTX_start()，再调用 BN_CTX_get() 获取临时 BIGNUM 变量，
	bn_x = BN_CTX_get(ctx);   //可以多次调用 BN_CTX_get()，最后调用 BN_CTX_end()。
	bn_y = BN_CTX_get(ctx);    //调用过 BN_CTX_end() 后 BN_CTX 结构体中的 BIGNUM 变量值将变得无效。
	
	if (!(bn_y))
	{
		goto clear_up;
	}
	if (!(group = EC_GROUP_new_by_curve_name(NID_sm2))) //构建一个内置曲线
	{
		goto clear_up;
	}
	if (!(bn_order = EC_GROUP_get0_order(group)))  //获取阶
	{
		goto clear_up;
	}
	if (!(ec_pt = EC_POINT_new(group)))   //椭圆曲线上得点
	{
		goto clear_up;
	}
	

	do
	{
		if (!(BN_rand_range(bn_d, bn_order)))
		{
			goto clear_up;
		}

	} while (BN_is_zero(bn_d));
	if (!(EC_POINT_mul(group, ec_pt, bn_d, NULL, NULL, ctx)))
	{
		goto clear_up;
	}
	if (!(EC_POINT_get_affine_coordinates_GFp(
		group, ec_pt, bn_x, bn_y, ctx)
		))
	{
		goto clear_up;
	}
	if (BN_bn2binpad(bn_x, pub_key_x, sizeof(pub_key_x)) != sizeof(pub_key_x))
	{
		goto clear_up;
	}
        int i=0;
        for(i=0;i<32;i++)
        printf("0x%x\n",pub_key_x[i]);
        pub_key_x[i]='\0';
        printf("公钥的x坐标为%s",pub_key_x);
	if (BN_bn2binpad(bn_y, pub_key_y, sizeof(pub_key_y)) != sizeof(pub_key_y))
	{
		goto clear_up;
	}
	//key_pair_test->pub_key[0] = 0x4;
	//memccpy((key_pair->pri_key+1),pub_key_x,sizeof(pub_key_x));
	//memcpy((key_pair_test->pub_key + 1), pub_key_x, sizeof(pub_key_x));
	//memcpy((key_pair_test->pub_key + 1 + sizeof(pub_key_x)), pub_key_y, sizeof(pub_key_y));
        
clear_up:
	if (ctx)
	{
		BN_CTX_end(ctx);
		BN_CTX_free(ctx);
	}
	if (group)
	{
		EC_GROUP_free(group);
	}
	if (ec_pt)
	{
		EC_POINT_free(ec_pt);
	}
	return 0;
}
