#include <string.h>
#include <stdio.h>
#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/objects.h>
#include <openssl/err.h>
 
int    main()
{
      EC_KEY                *key1,*key2;
      EC_POINT            *pubkey1,*pubkey2; //公钥
      BIGNUM              *prikey1,*prikey2;//私钥
      EC_GROUP           *group1,*group2;
      int                 ret,nid,size,i,sig_len;
      EC_builtin_curve     *curves;
      int                         crv_len;
      int                         len1,len2;
      
      /* 构造EC_KEY数据结构 */
      key1=EC_KEY_new();
      if(key1==NULL)
      {
             printf("EC_KEY_new err!\n");
             return -1;
      }
      key2=EC_KEY_new();
      if(key2==NULL)
      {
             printf("EC_KEY_new err!\n");
             return -1;
      }
      /* 获取实现的椭圆曲线个数 */
      crv_len = EC_get_builtin_curves(NULL, 0);
      printf("crv_len=%d\n",crv_len);
      curves = (EC_builtin_curve *)malloc(sizeof(EC_builtin_curve) * crv_len);
      /* 获取椭圆曲线列表 */
      EC_get_builtin_curves(curves, crv_len);
      /*
      nid=curves[0].nid;会有错误，原因是密钥太短
      */
      /* 选取一种椭圆曲线 */
      nid=curves[81].nid;
      printf("nid=%d\n",nid);
      /* 根据选择的椭圆曲线生成密钥参数group */
      group1=EC_GROUP_new_by_curve_name(nid);
      if(group1==NULL)
      {
             printf("EC_GROUP_new_by_curve_name err!\n");
             return -1;
      }
      group2=EC_GROUP_new_by_curve_name(nid);
      if(group2==NULL)
      {
             printf("EC_GROUP_new_by_curve_name err!\n");
             return -1;
      }
      /* 设置密钥参数  设置两组密钥参数 */
      ret=EC_KEY_set_group(key1,group1);
      if(ret!=1)
      {
             printf("EC_KEY_set_group err.\n");
             return -1;
      }
      ret=EC_KEY_set_group(key2,group2);
      if(ret!=1)
      {
             printf("EC_KEY_set_group err.\n");
             return -1;
      }
      /* 生成密钥 生成key1,key2*/
      ret=EC_KEY_generate_key(key1);
      if(ret!=1)
      {
             printf("EC_KEY_generate_key1 err.\n");
             return -1;
      }
      ret=EC_KEY_generate_key(key2);
      if(ret!=1)
      {
             printf("EC_KEY_generate_key2 err.\n");
             return -1;
      }
      /* 检查密钥 */
      ret=EC_KEY_check_key(key1);
      if(ret!=1)
      {
             printf("check key err.\n");
             return -1;
      }
      ret=EC_KEY_check_key(key2);
      if(ret!=1)
      {
             printf("check key err.\n");
             return -1;
      }
      /* 获取密钥大小 */
      size=ECDSA_size(key1);
      printf("key1的大小为 %d字节 \n",size);
      size=ECDSA_size(key2);
      printf("key2的大小为 %d字节 \n",size);
      /* 获取对方公钥，不能直接引用 */
      //获取key1的公钥
      pubkey1 = EC_KEY_get0_public_key(key1);
      //printf("%d\n",*((* key1).pub_key).x);
      
      char p1[100];
      char p2[100];
      char p3[100];
      BIGNUM *x = BN_new();
      BIGNUM *y = BN_new();
      if (EC_POINT_get_affine_coordinates_GFp(group1, pubkey1, x, y, NULL)) {
	printf("打印出key1的公钥的x坐标");
	strcpy(p1,BN_bn2hex(x));
	printf("%s\n",p1);
	printf("打印出key1的公钥的y坐标");
	strcpy(p2,BN_bn2hex(y));
        printf("%s\n",p2);
	unsigned char pubbuf0[1024] = { 0 };//公钥数据
        int buflen0 = EC_POINT_point2oct(group1, pubkey1, EC_KEY_get_conv_form(key1), pubbuf0, sizeof(pubbuf0), NULL);
        printf("公钥长度为%d字节=%d bits\n",buflen0,buflen0*8);
	
    }
      int bits;
       //获取key1的私钥
      prikey1 = EC_KEY_get0_private_key(key1);
      printf("打印出key1的私钥");
      strcpy(p3,BN_bn2hex(prikey1));
      printf("%s\n",p3);
      bits= BN_num_bits(prikey1);
      printf("key1的私钥一共%d位\n",bits);
      
      //获取key2的公钥
      pubkey2 = EC_KEY_get0_public_key(key2);
      
      char p4[100];
      char p5[100];
      char p6[100];
      if (EC_POINT_get_affine_coordinates_GFp(group2, pubkey2, x, y, NULL)) {
        printf("打印出key2的公钥的x坐标");
        strcpy(p4,BN_bn2hex(x));
        printf("%s\n",p4);
        printf("打印出key2的公钥的y坐标");
        strcpy(p5,BN_bn2hex(y));
        printf("%s\n",p5);
        unsigned char pubbuf[1024] = { 0 };//公钥数据	
	int buflen = EC_POINT_point2oct(group2, pubkey2, EC_KEY_get_conv_form(key2), pubbuf, sizeof(pubbuf), NULL);
	printf("公钥长度为%d字节=%d bits\n",buflen,buflen*8);
    }
      //获取key2的私钥
      prikey2 = EC_KEY_get0_private_key(key2);
      printf("打印出key2的私钥");
       strcpy(p6,BN_bn2hex(prikey2));
      printf("%s\n",p6);
      bits= BN_num_bits(prikey2);
      printf("key2的私钥一共%d位\n",bits);
      EC_KEY_free(key1);
      EC_KEY_free(key2);
      free(curves);
      return 0;
}
