#include<stdio.h>
# include <stdlib.h>
#include<string.h>
#include "sdf.h"
#include<time.h>
#include <openssl/bn.h>
#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/objects.h>
#include <openssl/err.h>
#include <openssl/ecdh.h>
#include<openssl/obj_mac.h>
int SDF_OpenDevice( void * * phDeviceHandle)
{
	return SDR_OK;
}
int SDF_CloseDevice( void * hDeviceHandle)
{
	return SDR_OK;
}
int SDF_GetDeviceInfo(void * hSessionHandle,DEVICEINFO * pstDeviceInfo)
{
	DEVICEINFO di;
	strcpy(di.IssuerName,"cindy");
	strcpy(di.DeviceName,"SDFcindy");
	strcpy(di.DeviceSerial,"20200422");
	di.DeviceVersion=1;
	(*pstDeviceInfo)= di;	
	
	return SDR_OK;
}
int SDF_GenerateRandom (void * hSessionHandle,unsigned int uiLength,unsigned char * pucRandom)
{
	BIGNUM *bn;
        
        int i;
        bn = BN_new(); //生成一个BIGNUM结构

        //int bits = 20;
        int top = -1;
        int bottom = 1;

        BN_rand(bn, uiLength, top, bottom); //生成指定bits的随机数

        char *a = BN_bn2hex(bn); //转化成16进制字符串
        //puts(a);
        //printf("\n");
        for(i=0;*(a+i)!='\0';i++)
        {
            *(pucRandom+i)=*(a+i);
        }

        *(pucRandom+i)='\0';
        BN_free(bn); //释放BIGNUM结构
        return SDR_OK;
}
//导出ECC签名公钥
int SDF_ExportSignPublicKey_ECC(void * hSessionHandle,unsigned int uiKeyIndex,ECCrefPublicKey * pucPublicKey){
    FILE *fp;  // 文件指针
    unsigned int KeyIndex;
    unsigned int Bits;
    unsigned char X[ECCref_MAX_LEN];
    unsigned char Y[ECCref_MAX_LEN];
    // 判断文件是否能够正确创建/打开
    if( (fp=fopen(FILE_PATH,"r")) == NULL ){
        perror(FILE_PATH);
        exit(1);
    }
    //printf("\n读取密钥容器（文件）中的签名公钥并取出来（放到key中）：\n");


   while(fscanf(fp, "%d\t%d\t%s\t%s", &KeyIndex, &Bits, X, Y) != EOF){ //fscanf是从文件中读取内容的啦
       // 挑选一下，我想要签名公钥
        if(KeyIndex == SGD_PUBLIC_KEY_SIGN){
          //printf("%d\t%d\t%s\t%s\n", KeyIndex, Bits, X, Y); //打印到显示器
          (*pucPublicKey).bits=Bits;
          strcpy((*pucPublicKey).x,X);
          strcpy((*pucPublicKey).y,Y); 
        }
    }
    fclose(fp);
    return SDR_OK;
}

//导出ECC加密公钥
int SDF_ExportEncPublicKey_ECC(void * hSessionHandle,unsigned int uiKeyIndex,ECCrefPublicKey * pucPublicKey){
        
    FILE *fp;  // 文件指针
    unsigned int KeyIndex;
    unsigned int Bits;
    unsigned char X[ECCref_MAX_LEN];
    unsigned char Y[ECCref_MAX_LEN];
    // 判断文件是否能够正确创建/打开
    if( (fp=fopen(FILE_PATH,"r")) == NULL ){
        perror(FILE_PATH);
        exit(1);
    }
   // printf("\n读取密钥容器（文件）中的加密公钥并取出来（放到key1中）：\n");

   
   while(fscanf(fp, "%d\t%d\t%s\t%s", &KeyIndex, &Bits, X, Y) != EOF){ //fscanf是从文件中读取内容的啦
       // 挑选一下，我想要签名公钥
        if(KeyIndex == SGD_PUBLIC_KEY_ENCRYPT){
          //printf("%d\t%d\t%s\t%s\n", KeyIndex, Bits, X, Y); //打印到显示器
          (*pucPublicKey).bits=Bits;
          strcpy((*pucPublicKey).x,X);
          strcpy((*pucPublicKey).y,Y); 
        }
    }
    fclose(fp);
    return SDR_OK;
}
/*产生ECC密钥对并输出*/
int SDF_GenerateKeyPair_ECC(void * hSessionHandle,unsigned int uiAlgID,unsigned int uiKeyBits,ECCrefPublicKey * pucPublicKey,ECCrefPrivateKey * pucPrivateKey)
{
        
	BN_CTX* ctx = NULL;
	BIGNUM* bn_d = NULL, * bn_x = NULL, * bn_y = NULL;
	const BIGNUM* bn_order;
	EC_GROUP* group = NULL;
	EC_POINT* ec_pt = NULL;
        unsigned char pri_key[32];
	unsigned char pub_key_x[32], pub_key_y[32];
	if (!(ctx = BN_CTX_secure_new()))
	{
		goto clean_up;
	}
	BN_CTX_start(ctx);
	bn_d = BN_CTX_get(ctx);
	bn_x = BN_CTX_get(ctx);
	bn_y = BN_CTX_get(ctx);
	if (!(bn_y))
	{
		goto clean_up;
	}

	if (!(group = EC_GROUP_new_by_curve_name(NID_sm2)))
	{
		goto clean_up;
	}
	if (!(bn_order = EC_GROUP_get0_order(group)))
	{
		goto clean_up;
	}
	if (!(ec_pt = EC_POINT_new(group)))
	{
		goto clean_up;
	}
	do
	{
		if (!(BN_rand_range(bn_d, bn_order)))
		{
			goto clean_up;
		}
	} while (BN_is_zero(bn_d));

	if (!(EC_POINT_mul(group, ec_pt, bn_d, NULL, NULL, ctx)))
	{
		goto clean_up;
	}
	if (!(EC_POINT_get_affine_coordinates_GFp(group,
		ec_pt,
		bn_x,
		bn_y,
		ctx)))
	{
		goto clean_up;
	}
        if (BN_bn2binpad(bn_d,pri_key,
		sizeof(pri_key)) != sizeof(pri_key))
	{
		goto clean_up;
	}
        if (BN_bn2binpad(bn_x,
		pub_key_x,
		sizeof(pub_key_x)) != sizeof(pub_key_x))
	{
		goto clean_up;
	}
        if (BN_bn2binpad(bn_y,
		pub_key_y,
		sizeof(pub_key_y)) != sizeof(pub_key_y))
	{
		goto clean_up;
	}

        //下面打印一下密钥进行测试~~~
        //printf("打印一下私钥");
        int i;
        int j=1;
        //printf("\n");
        for(i=0;i<32;i++){
          //printf("0x%x",pri_key[i]);
          if(pri_key[i]%16>=0 && pri_key[i]%16 <=9)
            (*pucPrivateKey).K[j]=pri_key[i]%16 + '0';
          else
            (*pucPrivateKey).K[j]= 'a'-10 + pri_key[i]%16;
          
          pri_key[i]=pri_key[i]/16;
          if(pri_key[i]%16>=0 && pri_key[i]%16 <=9)
            (*pucPrivateKey).K[j-1]=pri_key[i]%16 + '0';
          else
            (*pucPrivateKey).K[j-1]= 'a'-10 + pri_key[i]%16;
          
          j=j+2;
       
         }
         (*pucPrivateKey).K[j-1]='\0';
 
        //公钥的x坐标
        j=1;
        //printf("打印一下公钥的x坐标");
        for(i=0;i<32;i++){
          //printf("0x%x",pub_key_x[i]);
          if(pub_key_x[i]%16>=0 && pub_key_x[i]%16 <=9)
            (*pucPublicKey).x[j]=pub_key_x[i]%16 + '0';
          else
            (*pucPublicKey).x[j]= 'a'-10 + pub_key_x[i]%16;
          
          pub_key_x[i]=pub_key_x[i]/16;
         if(pub_key_x[i]%16>=0 && pub_key_x[i]%16 <=9)
            (*pucPublicKey).x[j-1]=pub_key_x[i]%16 + '0';
          else
            (*pucPublicKey).x[j-1]= 'a'-10 + pub_key_x[i]%16;
          
          j=j+2;
       
          
         }
        //printf("\n");
         (*pucPublicKey).x[j-1] = '\0';
       // printf("再此打印一下公钥的x坐标");
       // printf("%s\n",(*pucPublicKey).x);
        
        //下面打印公钥进行测试
        j=1;
        //printf("打印一下公钥的y坐标");
        for(i=0;i<32;i++){
          //printf("0x%x",pub_key_y[i]);
          if(pub_key_y[i]%16>=0 && pub_key_y[i]%16 <=9)
            (*pucPublicKey).y[j]=pub_key_y[i]%16 + '0';
          else
            (*pucPublicKey).y[j]= 'a'-10 + pub_key_y[i]%16;
          
          pub_key_y[i]=pub_key_y[i]/16;
         if(pub_key_y[i]%16>=0 && pub_key_y[i]%16 <=9)
            (*pucPublicKey).y[j-1]=pub_key_y[i]%16 + '0';
          else
            (*pucPublicKey).y[j-1]= 'a'-10 + pub_key_y[i]%16;
          
          j=j+2;
       
          
         }
        //printf("\n");
         (*pucPublicKey).y[j-1] = '\0';
        //printf("再此打印一下公钥的y坐标");
        //printf("%s\n",(*pucPublicKey).y);
	//位数信息
        (*pucPublicKey).bits=256;
        (*pucPrivateKey).bits=256;
	
        return SDR_OK;	
clean_up:
	if (ctx)
	{
		BN_CTX_end(ctx);
		BN_CTX_free(ctx);
	}

	if (group)
	{
		EC_GROUP_free(group);
	}

	if (ec_pt)
	{
		EC_POINT_free(ec_pt);
	}
        return SDR_OK;
      
}
