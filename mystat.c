#include<stdio.h>
#include<time.h>
#include<sys/stat.h>
#include<unistd.h>
#include<sys/types.h>
#include<errno.h>
#include<stdlib.h>
 
int main(int argc, char **argv)
{
    struct stat buf;
    /*检查参数个数*/
    if(argc!=2)
    {
        perror("Usage:my_stat <filename>\n");
        exit(1);
    }
 
    /*获取文件属性*/
    if( stat(argv[1], &buf) ==-1 )
    {
        perror("stat:");
        exit(1);
    }
 
    /*打印文件属性*/
    printf("设备 %d\n",buf.st_dev);//文件设备编号
    printf("文件大小: %d\n",buf.st_size);//文件大小，以字节计算
    printf("I/O: %d\n",buf.st_blksize);//文件系统ＩＯ缓冲区大小
    printf("块个数: %d\n",buf.st_blocks);//占有文件区块个数，一般一个区块大小通常５１２字节
    printf("Inode : %d\n",buf.st_ino);//文件ｉ节点标号
    printf("mode: %o\n",buf.st_mode);//文件类型与存取权限
    printf("硬链接: %d\n",buf.st_nlink);//硬链接数目
    printf("UID : %d\n",buf.st_uid);//用户所有者的用户ｉｄ
    printf("GID: %d\n",buf.st_gid);//用户所有者组ｉｄ
    printf("最近访问: %s",ctime(&buf.st_atime));//文件最近一次被访问时间
    printf("最近更改: %s",ctime(&buf.st_mtime));//最后一次被修改的时间
    printf("最近改动: %s",ctime(&buf.st_ctime));//最近一次被更改时间
 
    return 0;
}


